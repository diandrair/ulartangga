package finalAssignmentDiandra;

import java.util.HashMap;


import abstractBase.BoardEntity;



public class Board {

	HashMap<Integer, BoardEntity> cells;
	int cellCount;
	
	public Board(int dimention) {
		this.cellCount = dimention*dimention;
		cells = new HashMap<Integer, BoardEntity>();
		for(int i=0; i<dimention; i++) {
			int min = 2;
			int max = cellCount - 1;
			int start = (int) Math.floor(Math.random()*(max-min+1)+min);
			max = start - 1;
			int end = (int) Math.floor(Math.random()*(max-min+1)+min);
			if(!hasSnakeOrLadder(start)) {
				setEntity(start, new Snake(start, end));
			}
			max = cellCount - 1;
			end = (int) Math.floor(Math.random()*(max-min+1)+min);
			max = end - 1;
			start = (int) Math.floor(Math.random()*(max-min+1)+min);;
			if(!hasSnakeOrLadder(start)) {
				setEntity(start, new Ladder(start, end));
			}
			
		}
	}
	
	public boolean hasSnakeOrLadder(int cellIndex) {
		return cells.containsKey(cellIndex);
	}
	
	private void setEntity(int index, BoardEntity e) {
		cells.put(index, e);
	}
	
	public BoardEntity getEntity(int index) {
		if(hasSnakeOrLadder(index)) {
			return this.cells.get(index);
		}
		return null;
	}

	
	

}
