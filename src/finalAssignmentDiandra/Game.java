package finalAssignmentDiandra;


import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;



public class Game {

	Snake snake;
	Dice Dice;
	Board board;
	Queue<Player> players;
	Queue<Player> winners;
	int diceCount;
	int dimention;
	int size;
	int numberOfPlayers;
	Scanner sc = new Scanner(System.in);
	
	public Game(int dimention) {
		this.size = dimention*dimention;
		this.dimention = dimention;
		board = new Board(dimention);
		players = new LinkedList<Player>();
		winners = new LinkedList<Player>();
		

	}
	
	public void startGame() {
        while(players.size() > 0) {
            Player currentPlayer = players.poll();
            System.out.println(currentPlayer.getUserName()+"'s turn.");
            move(currentPlayer);
            if (currentPlayer.getPosition() == size) {
				System.out.println(currentPlayer.getUserName() + " won!!!");
				winners.add(currentPlayer);
			} else {
				players.add(currentPlayer);
			}
			printPositions();
		}
     }
	
	private void printPositions() {
		for(Player player : players) {
			System.out.println(player.getUserName()+":"+player.getPosition());
		}
		
	}
	
	private void move(Player player) {
		int diceValue = Dice.roll(diceCount);
        int position = player.getPosition();
        System.out.println("You got: "+ diceValue);
        int finalPos = position+diceValue ;
        if(finalPos <= size) position += diceValue;
        player.setPosition(finalPos);
        System.out.println(player.getUserName() + " moved to " + position);
    }
	
	public Queue<Player> getPlayers() {
		return players;
	}

	public void addPlayer(Player player) {
		
		
		this.players.add(player);
	}
	
	 public void addPlayers() {
         System.out.print("Enter number of players : ");
         numberOfPlayers = sc.nextInt();
         
         System.out.println("What are the names of the " + numberOfPlayers + " players?");
         for(int loop = 1; loop <= numberOfPlayers; loop++) {
             Player player = new Player();
             
             System.out.print("Enter name for player " + loop + ": ");
             player.setUserName(sc.next());
             
             players.add(player);
         }
         sc.close();
     }

	public Queue<Player> getWinners() {
		return winners;
	}

	public void setWinners(Queue<Player> winners) {
		this.winners = winners;
	}

	public int getDiceCount() {
		return diceCount;
	}

	public void setDiceCount(int diceCount) {
		this.diceCount = diceCount;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}
	
	
}

