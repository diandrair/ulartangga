package finalAssignmentDiandra;

import abstractBase.BoardEntity;

public class Ladder extends BoardEntity{

	public Ladder(int start, int end) {
		super(start, end);
	}

	public String getEncounterMessage() {
		return "Hurrayy!! I am going up :) ";
	}

	public String getString() {
		return "L("+this.getEnd()+")  ";
	}
	



}
