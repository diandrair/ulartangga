package finalAssignmentDiandra;

import abstractBase.BoardEntity;

public class Snake extends BoardEntity{

	public Snake(int start, int end) {
		super(start, end);
		
	}

	public String getEncounterMessage() {
		return "Ooops!! You encountered a Snake :( ";
		
	}

	public String getString() {	
	
		return "S("+this.getEnd()+")  ";
	}


}
